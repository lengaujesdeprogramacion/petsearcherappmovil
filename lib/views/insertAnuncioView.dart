import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:pet_searcher/obj/anuncio.dart';
import 'package:pet_searcher/obj/pet.dart';
import 'package:pet_searcher/widgets/viewWidgets.dart';

class InsertAnuncioView extends StatelessWidget{
  BuildContext context;
  String userId;
  InsertAnuncioView(this.context,this.userId);
  final TextEditingController tituloController=TextEditingController();
  final TextEditingController descripcionController=TextEditingController();
  final TextEditingController categoriaController=TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(15.0, 30.0, 0.0, 0.0),
                child: Text(
                  'INSERT ANUNCIO ',
                  style:
                    TextStyle(fontSize: 80.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ]
            ),
          ),
          Container( 
          padding: EdgeInsets.fromLTRB(10, 0, 30, 0),
          child: Column(
            children: <Widget>[
                        ViewWidget.textInput("TITULO",tituloController),
                        SizedBox(height: 8.0),
                        ViewWidget.textInput("DESCRIPCION",descripcionController),                 
                        SizedBox(height: 8.0),
                        ViewWidget.textInput("CATEGORIA",categoriaController),            
                        SizedBox(height: 20.0),
                        ViewWidget.botonWidget("Publicar Anuncio", _publicarMascota,200),
                        SizedBox(height: 30.0),
                        ViewWidget.botonWidget("Ir atras.", _salir,200),
              ],
            ),
          ),
        ], 
      ),
    );
  }
  void _publicarMascota() async{
    Anuncio anuncio=Anuncio.insertar(tituloController.text,descripcionController.text,categoriaController.text,userId);
    String url = 'http://127.0.0.1:8080/anuncios/post';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{"titulo": "${anuncio.titulo}", "descripcion": "${anuncio.descripcion}", "categoria":"${anuncio.categoria}", "user_id":"${anuncio.user_id}"}';
    
    // make POST request
    Response response = await post(url, headers: headers, body: json);
    // check the status code for the result
    int statusCode = response.statusCode;
    // this API passes back the id of the new item added to the body
    if(statusCode==404){
      ViewWidget.showErrorDialog(context, "Error de conexion", "No hay conexion con el servidor");
      return null;
    }
    Navigator.pop(context);
  }
  void _salir(){
    Navigator.pop(context);
  }


}