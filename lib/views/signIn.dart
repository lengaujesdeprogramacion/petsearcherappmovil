import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pet_searcher/obj/user.dart';
import 'package:pet_searcher/views/listViewPets.dart';
import 'package:pet_searcher/views/signup.dart';
import 'package:pet_searcher/widgets/viewWidgets.dart';
import 'package:http/http.dart' as http;

class SignInView extends StatefulWidget{
  SignInView();
  @override
  State<StatefulWidget> createState() {
    return _SignInViewState();
  }
  
}

class _SignInViewState extends State<SignInView>{
  
  final TextEditingController userController=TextEditingController();
  final TextEditingController contrasenaController=TextEditingController();
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
           Container(
            child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(15.0, 100.0, 0.0, 0.0),
                child: Text(
                  'SIGN UP ',
                  style:
                    TextStyle(fontSize: 80.0, fontWeight: FontWeight.bold),
                  ),
            ),
            ]
           ),
           ),
          Container(
              padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[
                  ViewWidget.textInput("USUARIO",userController),
                  SizedBox(height: 8.0),
                  ViewWidget.textInput("CONTRASENA",contrasenaController),                 
                  SizedBox(height: 80.0),
                  ViewWidget.botonWidget("Iniciar Sesion", _logInButtonFunc,200),
                  SizedBox(height: 40.0),
                  ViewWidget.botonWidget("Registrate.", _tapRegistrarse,200),
                ],
              )
              ),     
              ],
            ),
          );
  }
  void _tapRegistrarse(){
    Route route = MaterialPageRoute(builder: (context) => SignupPage());
    Navigator.push(context,route);
  }
  void _logInButtonFunc() async{
    try{
      User user;
      var userData=await http.get("http://127.0.0.1:8080/users/${userController.text}/${contrasenaController.text}");
      var userJson= json.decode(userData.body);
      user=User(userJson["user_id"],userJson["first_name"],userJson["last_name"],
      userJson["phone_number"],userJson["city"],userJson["country"],userJson["usuario"],
      userJson["contrasena"],userJson["foto"]);
      Route route=MaterialPageRoute(builder: (context)=>ListViewPets(user,context));
      Navigator.push(context,route);
    }
    catch(e){
      ViewWidget.showErrorDialog(context, "Error de inicio de sesion", "Usuario y/o contraseña incorrectas");
    }
    
    }
    
   
}
