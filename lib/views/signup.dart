import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:pet_searcher/obj/user.dart';
import 'package:pet_searcher/views/signIn.dart';
import 'package:pet_searcher/widgets/viewWidgets.dart';

class SignupPage extends StatefulWidget {
  @override
  SignupPageState createState() => SignupPageState();
}

class SignupPageState extends State<SignupPage> {
  final nombreController = TextEditingController();
  final apellidoController = TextEditingController();
  final usuarioController = TextEditingController();
  final contrasenaController = TextEditingController();
  final telefonoController = TextEditingController();
  final ciudadController = TextEditingController();
  final paisController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(

        resizeToAvoidBottomPadding: false,
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, 
          children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(15.0, 100.0, 0.0, 0.0),
                  child: Text(
                    'SIGN UP ',
                    style:
                        TextStyle(fontSize: 80.0, fontWeight: FontWeight.bold),
                  ),
                ),
                
              ],
            ),
          ),
          Container(
              padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[
                  ViewWidget.textInput("NOMBRES",nombreController),
                  SizedBox(height: 8.0),
                  ViewWidget.textInput("APELLIDOS",apellidoController),
                  SizedBox(height: 8.0),
                  ViewWidget.textInput("USUARIO",usuarioController),
                  SizedBox(height: 8.0),
                  ViewWidget.textInput("CONTRASENA",contrasenaController),
                  SizedBox(height: 8.0),
                  ViewWidget.textInput("TELEFONO",telefonoController),
                  SizedBox(height: 8.0),
                  ViewWidget.textInput("CIUDAD",ciudadController),
                  SizedBox(height: 8.0),
                  ViewWidget.textInput("PAIS",paisController),
                  SizedBox(height: 20.0),
                  ViewWidget.botonWidget("Registrarse", onTapRegistrarse, 300),
                  SizedBox(height: 40.0),
                  ViewWidget.botonWidget("Ir Atras", ()=>Navigator.pop(context), 300),
                ],
              )
              ), 
        ]
        )
        );
  }

  void onTapRegistrarse() async{
    User usuario= User.insertar(nombreController.text,apellidoController.text,telefonoController.text,ciudadController.text,paisController.text,usuarioController.text,contrasenaController.text);
    String url = 'http://127.0.0.1:8080/users/post';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = '{"first_name": "${usuario.nombre}", "last_name": "${usuario.apellido}", "phone_number":"${usuario.telefono}", "city":"${usuario.city}", "usuario":"${usuario.usuario}", "contrasena":"${usuario.contrasena}"}';
    // make POST request
    Response response = await post(url, headers: headers, body: json);
    // check the status code for the result
    int statusCode = response.statusCode;
    // this API passes back the id of the new item added to the body
    if(statusCode==404){
      ViewWidget.showErrorDialog(context, "Error de conexion", "No hay conexion con el servidor");
      return null;
    }
    Navigator.of(context).pop();
    Route route = MaterialPageRoute(builder: (context) => SignInView());
    Navigator.push(context,route);
  }
  

}