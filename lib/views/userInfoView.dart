import 'package:flutter/material.dart';
import 'package:pet_searcher/obj/user.dart';
import 'package:pet_searcher/widgets/viewWidgets.dart';


class UserInfoView extends StatelessWidget{
  final User user;
  final BuildContext context;
  UserInfoView(this.user,this.context);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          _topContent(),
          _bottomContent(),
        ],
      ),
      

     );
  }
  Widget _topContent(){
    return Stack(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(left: 10.0),
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new NetworkImage(user.urlFoto),
                fit: BoxFit.cover,
              ),
            )),
        Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: _topContentText(),
          ),
        ),
        Positioned(
          left: 8.0,
          top: 60.0,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          ),
        )
      ],
    );
  }
  Widget _topContentText(){
     return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 120.0),
        Icon(
          Icons.pets,
          color: Colors.white,
          size: 40.0,
        ),
        Container(
          width: 90.0,
          child: new Divider(color: Colors.green),
        ),
        SizedBox(height: 10.0),
        Text(
          "${user.nombre} ${user.apellido}",
          style: TextStyle(color: Colors.white, fontSize: 45.0),
        ),
        SizedBox(height: 30.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(flex: 3, child: Text(user.city,
            style: TextStyle(color: Colors.white),
            )
            ),
            Expanded(
                flex: 6,
                child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(
                      user.country,
                      style: TextStyle(color: Colors.white),
                    ))),
            Expanded(flex: 1, child: _idUser())
          ],
        ),
      ],
    );
  }
  Widget _idUser(){
    return Container(
      padding: const EdgeInsets.all(7.0),
      decoration: new BoxDecoration(
          border: new Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5.0)),
      child: new Text(
        // "\$20",
        "${user.userId}",
        style: TextStyle(color: Colors.white),
      ),
    );
  }
  Widget _bottomContent(){
    return Container(
      // height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      // color: Theme.of(context).primaryColor,
      padding: EdgeInsets.all(40.0),
      child: Center(
        child: Column(
          children: <Widget>[_bottomContentText(),SizedBox(height: 30,), ViewWidget.botonWidget("Comunicarse con usuario", ()=>{}, 200)],
        ),
      ),
    );
  }
  Widget _bottomContentText(){
     return Text(
      user.telefono,
      style: TextStyle(fontSize: 18.0),
    );
  }
  
}