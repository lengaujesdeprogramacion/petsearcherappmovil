import 'package:flutter/material.dart';

class ViewWidget{
  static Widget textInput(String texto,TextEditingController controller){
    return TextField(
      controller: controller,
      decoration: InputDecoration(
      labelText: texto,
      labelStyle: TextStyle(
        fontFamily: 'Montserrat',
        fontWeight: FontWeight.bold,
      color: Colors.grey),
      focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.green))),
                  );
  }

  static Widget botonWidget(String texto, Function ontap,double width){
    return Container(
      width: width,
      height: 40.0,
      child: Material(
        
        borderRadius: BorderRadius.circular(20),
        shadowColor: Colors.brown,
        color: Colors.brown,
        elevation: 7.0,
        child: GestureDetector(
          onTap: ontap,
          child: Center(
            child: Text(
              texto,
              style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Montserrat'),
            ),
          ),
        ),
      )
    );
  }
  static void showErrorDialog(BuildContext context,String title,String body) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          
          title: new Text(title),
          content: new Text(body),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  static Widget logoPetSearcher(){
   return Row(
    children:<Widget>[
      SizedBox(width: 70,),
      Text("PetSearcher"),
      SizedBox(width: 5,),
      Icon(Icons.pets,
        color: Colors.white,
      )
    ]
  );
  }
}